#!/bin/bash
set -e
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
#git clone https://gitlab.com/willmac321/vimupgradescript.git
cp .vimrc ~/.vimrc 
vim -c 'PlugInstall' -c 'qa!'
