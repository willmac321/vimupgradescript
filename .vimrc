"let b:usemarks=1
set nocompatible              " be iMproved, required
filetype off                  " required

let g:ale_disable_lsp = 1

call plug#begin('~/.vim/plugged')
" " plugin on GitHub repo
" Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}
" Plug 'neoclide/coc.nvim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'tpope/vim-fugitive'

Plug 'scrooloose/nerdtree'

Plug 'Xuyuanp/nerdtree-git-plugin'

Plug 'tpope/vim-surround'

Plug 'tpope/vim-commentary'

Plug 'tpope/vim-obsession'

Plug 'dhruvasagar/vim-prosession'

"VUE plugin for vim
"Plug 'posva/vim-vue'

"vim colorscheme pack
Plug 'flazz/vim-colorschemes'

Plug 'w0rp/ale'

Plug 'godlygeek/tabular'

Plug 'kien/ctrlp.vim'

Plug 'sheerun/vim-polyglot'

Plug 'airblade/vim-gitgutter'

Plug 'ap/vim-css-color'

Plug 'heavenshell/vim-jsdoc', { 
  \ 'for': ['javascript', 'javascript.jsx','typescript', 'typescript.jsx'], 
  \ 'do': 'make install'
  \}

Plug 'prettier/vim-prettier', { 'do': 'yarn install --frozen-lockfile --production' }

"Plug 'codota/tabnine-nvim', { 'do': './dl_binaries.sh' }

"Plug 'habamax/vim-godot'

Plug 'neoclide/jsonc'

call plug#end()

filetype on
filetype plugin indent on

syntax enable

set mouse=nicr

let NERDTreeShowHidden=1

set number
set ruler
colorscheme gruvbox 
set background=dark
set tabstop=2
set shiftwidth=2
set expandtab
set hlsearch
set incsearch
set cursorline
set showmatch
set foldmethod=syntax
set nofoldenable

" to always show status line
set laststatus=2

" For vimgrep
set wildignore+=node_modules/**,*/node_modules/**,app/public/**,*/app/public/**,*/vendor/*,vendor/**,**.gz

set statusline=\ %l/%L:%c\ [%p%%]\ \%m\ %.60f

let g:vimspector_enable_mappings = 'HUMAN'

let g:ale_virtualenv_dir_names =[
      \   '.venv/3.8.7',
      \   '.venv',
      \]

let g:NERDTreeShowLineNumbers=0
" temp since quick fix gets weird size
let g:NERDTreeMinimalMenu=1
let g:ale_sign_error = '✘'
let g:ale_sign_warning = '⚠'
highlight ALEErrorSign ctermbg=NONE ctermfg=red
highlight ALEWarningSign ctermbg=NONE ctermfg=yellow
highlight CocHintSign ctermbg=NONE ctermfg=LightBlue

let b:ale_linter_aliases = {
  \'javascriptreact':['typescript','javascript','css'],
  \'jsx':['javascript', 'css', 'typescript'],
  \'vue':['javascript', 'vue']
  \}

let g:ale_fixers ={
      \ 'yaml':['yamlfix'],
      \ 'jsx':['prettier', 'eslint'],
			\	'json': ['prettier'],
			\	'javascript': ['prettier','eslint'],
			\	'javascriptreact': ['eslint'],
			\	'typescript': ['prettier', 'eslint'],
			\	'typescriptreact': ['prettier', 'eslint'],
      \ 'scss': ['prettier', 'eslint'],
      \ 'module.scss': ['prettier', 'eslint'],
      \ 'css': ['prettier', 'eslint'],
      \ 'vue': ['eslint'],
      \ 'python': ['black', 'isort', 'remove_trailing_lines', 'trim_whitespace'],
      \ 'sql': ['pgformatter'],
      \ 'php': ['php_cs_fixer'],
      \ 'html': ['prettier'],
      \ 'c': ['clang-format'],
      \ 'go': ['goimports', 'golines', 'gofumpt' ],
      \ 'ruby': ['rubocop' ],
      \ 'markdown': ['prettier' ],
			\}

let g:ale_linters ={ 
       \ 'yaml':['yamllint'],
       \ 'javascript': ['eslint'],
			 \ 'typescript': ['eslint'],
			 \ 'typescriptreact': ['eslint'],
       \ 'javascriptreact': ['eslint'],
       \ 'json': ['eslint'],
       \ 'jsx': ['eslint'],
       \ 'scss': ['prettier', 'eslint'],
       \ 'module.scss': ['prettier', 'eslint'],
       \ 'css': ['prettier', 'eslint'],
       \ 'html': ['prettier'],
       \ 'python': ['pylint'],
       \ 'sql': ['sql-lint'],
       \ 'vue' :['eslint', 'vls'],
       \ 'php' :['php_cs'],
       \ 'c': ['clangd'],
       \ 'go': ['gopls'],
       \}

call ale#linter#Define('gdscript', {
\   'name': 'godot',
\   'lsp': 'socket',
\   'address': '127.0.0.1:6008',
\   'project_root': 'project.godot',
\})

let g:jsdoc_allow_input_prompt = 1
let g:jsdoc_enable_es6 = 1

"let g:ale_fix_on_save = 1
"let g:ale_python_pylint_options = '--load-plugins pylint_django'

"jsdoc plugin keymap
nmap <silent> <C-l> <Plug>(jsdoc)

inoremap <c-s> <c-o>$
inoremap <c-a> <c-o>0

inoremap <s-tab> <c-d>

inoremap jj <Esc>
inoremap jk <Esc>
inoremap kj <Esc>

" move by visual line not file line
nnoremap j gj
nnoremap k gk

" autocmd vimenter * NERDTree 
" autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
autocmd Filetype py setl shiftwidth=4 softtabstop=4
autocmd Filetype py set foldmethod=indent

autocmd FileType * normal zR
autocmd FileType json set foldlevelstart=2

"coc config
let g:coc_global_extensions = [
  \ 'coc-snippets',
  \ 'coc-tsserver',
  \ 'coc-eslint', 
  \ 'coc-prettier', 
  \ 'coc-json', 
  \ 'coc-html',
  \ 'coc-css',
  \ 'coc-phpls',
  \ 'coc-pyright',
  \ 'coc-yaml',
  \ 'coc-clangd',
  \ 'coc-go',
  \ ]

"COC Readme
" TextEdit might fail if hidden is not set.
set hidden

"set encoding=utf-8

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.
set cmdheight=1

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

set backspace=indent,eol,start

" Always show the signcolumn, otherwise it would shift the text each time
" " diagnostics appear/become resolved.
if has("patch-8.1.1564")
"   " Recently vim can merge signcolumn and number column into one
   set signcolumn=number
else
   set signcolumn=yes
endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <C-l>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<C-l>" :
      \ coc#refresh()

inoremap <expr><S-C-l> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" " Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" " position. Coc only does snippet and additional edit on confirm.
" " <cr> could be remapped by other vim plugin, try `:verbose imap <CR>`.
" if exists('*complete_info')
"   inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
" else
"   inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" endif

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" if get annoying poppup on rename change coc.preferences.promptInput to
" false

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current line.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Introduce function text object
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap if <Plug>(coc-funcobj-i)
omap af <Plug>(coc-funcobj-a)

" Use CTRL-s for selections ranges.
" NOTE: Requires 'textDocument/selectionRange' support from the language server.
" coc-tsserver, coc-python are the examples of servers that support it.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Use CTRL-s for selections ranges.
" NOTE: Requires 'textDocument/selectionRange' support from the language server.
" coc-tsserver, coc-python are the examples of servers that support it.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Create a function to get word under curosr, wait for input,, and call CocSearch, needs ripgrep to work
function! SearchWithWordUnderCursor()
  let l:word = expand('<cword>')
  let l:input = input('Search: ', l:word)
  call histadd('cmd', 'CocSearch -i ' . l:input)
  execute 'CocSearch -i ' . l:input
endfunction

" map the func
nnoremap <leader>s :call SearchWithWordUnderCursor()<CR>

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline+=%=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings using CoCList:
" Show all diagnostics.
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>


"Add all TODO items to the quickfix list relative to where you opened Vim.
function! s:todo() abort
  let entries = []
  for cmd in ['git grep -niIw -e TODO -e FIXME 2> /dev/null',
            \ 'grep -rniIw -e TODO -e FIXME . 2> /dev/null']
    let lines = split(system(cmd), '\n')
    if v:shell_error != 0 | continue | endif
    for line in lines
      let [fname, lno, text] = matchlist(line, '^\([^:]*\):\([^:]*\):\(.*\)')[1:3]
      call add(entries, { 'filename': fname, 'lnum': lno, 'text': text })
    endfor
    break
  endfor

  if !empty(entries)
    call setqflist(entries)
    copen
  endif
endfunction

command! Todo call s:todo()

noremap <F4> <Esc>:Todo<CR>
noremap <leader>8 <Esc>:Todo<CR>
noremap <F5> <Esc>:NERDTreeToggle<CR>
noremap <leader>0 <Esc>:NERDTreeToggle<CR> 
noremap <F9> <Esc>:ALEFix<CR>
noremap <leader>9 <Esc>:ALEFix<CR>
nmap <leader>p a<CR><Esc>k$p$a <Esc>bJkJ

nnoremap <Leader>dd :set mouse=a <bar> :call vimspector#Launch()<CR>
nnoremap <Leader>de :set mouse-=a <bar> :call vimspector#Reset()<CR>
nnoremap <Leader>dc :call vimspector#Continue()<CR>

nnoremap <Leader>dt :call vimspector#ToggleBreakpoint()<CR>
nnoremap <Leader>dT :call vimspector#ClearBreakpoints()<CR>

nmap <Leader>dk <Plug>VimspectorRestart
nmap <Leader>dh <Plug>VimspectorStepOut
nmap <Leader>dl <Plug>VimspectorStepInto
nmap <Leader>dj <Plug>VimspectorStepOver

